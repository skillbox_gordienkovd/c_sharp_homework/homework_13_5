﻿using System.Windows.Input;
using homework_13_5.UI.Client.List;

namespace homework_13_5.UI.Home
{
    public partial class Home
    {
        public Home()
        {
            InitializeComponent();
        }

        private void NavigateClientList(object sender, MouseButtonEventArgs e)
        {
            var clientListPage = new ClientList();
            var navigationService = NavigationService;
            navigationService?.Navigate(clientListPage);
        }
    }
}