﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using homework_13_5.UI.Client.Add;
using homework_13_5.UI.Client.Details;
using homework_13_5.UI.Client.Edit;

namespace homework_13_5.UI.Client.List
{
    public partial class ClientList
    {
        private ListSortDirection _lastDirection = ListSortDirection.Ascending;
        private GridViewColumnHeader _lastHeaderClicked;
        
        public ClientList()
        {
            InitializeComponent();
            ClientListBox.ItemsSource = Repository.Repository.ClientRepository.Clients;
            ClientListBox.Items.Refresh();
        }

        private void EditClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            var clientEditPage = new ClientEdit(id);
            NavigationService?.Navigate(clientEditPage);
        }

        private void DetailsClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            var clientDetailsPage = new ClientDetails(id);
            NavigationService?.Navigate(clientDetailsPage);
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            Repository.Repository.ClientRepository.Delete(id);
            ClientListBox.Items.Refresh();
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            var clientAddPage = new ClientAdd();
            var navigationService = NavigationService;
            navigationService?.Navigate(clientAddPage);
        }
        
        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked == null) return;

            if (headerClicked.Role == GridViewColumnHeaderRole.Padding) return;

            if (headerClicked != _lastHeaderClicked)
                direction = ListSortDirection.Ascending;
            else
                direction = _lastDirection == ListSortDirection.Ascending
                    ? ListSortDirection.Descending
                    : ListSortDirection.Ascending;

            var columnBinding = headerClicked.Column.DisplayMemberBinding as Binding;
            var sortBy = columnBinding?.Path.Path ?? headerClicked.Column.Header as string;

            if (sortBy == null)
            {
                return;
            }
            
            ClientListBox.Items.SortDescriptions.Clear();
            var sd = new SortDescription(sortBy, direction);
            ClientListBox.Items.SortDescriptions.Add(sd);
            ClientListBox.Items.Refresh();


            _lastHeaderClicked = headerClicked;
            _lastDirection = direction;
        }
    }
}