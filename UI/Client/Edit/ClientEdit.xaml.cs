﻿using System.Windows;
using homework_13_5.UI.Client.List;

namespace homework_13_5.UI.Client.Edit
{
    public partial class ClientEdit
    {
        private readonly Domain.Client _client;

        public ClientEdit(long id)
        {
            _client = Repository.Repository.ClientRepository.FindById(id);
            InitializeComponent();
            ClientFirstname.Text = _client.Firstname;
            ClientLastname.Text = _client.Lastname;
        }

        private void Save(object sender, RoutedEventArgs routedEventArgs)
        {
            _client.Firstname = ClientFirstname.Text;
            _client.Lastname = ClientLastname.Text;
            Repository.Repository.ClientRepository.Save(_client);
            var clientList = new ClientList();
            NavigationService?.Navigate(clientList);
        }
    }
}