﻿using System.Windows;
using homework_13_5.UI.Client.List;

namespace homework_13_5.UI.Client.Add
{
    public partial class ClientAdd
    {
        private readonly Domain.Client _client = new Domain.Client();

        public ClientAdd()
        {
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            _client.Firstname = ClientFirstname.Text;
            _client.Lastname = ClientLastname.Text;
            Repository.Repository.ClientRepository.Save(_client);
            var clientList = new ClientList();
            NavigationService?.Navigate(clientList);
        }
    }
}