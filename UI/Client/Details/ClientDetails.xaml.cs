﻿using System.Windows;
using homework_13_5.UI.Account.List;

namespace homework_13_5.UI.Client.Details
{
    public partial class ClientDetails
    {
        private readonly Domain.Client _client;

        public ClientDetails(long id)
        {
            _client = Repository.Repository.ClientRepository.FindById(id);
            InitializeComponent();
            ClientFirstname.Text = _client.Firstname;
            ClientLastname.Text = _client.Lastname;
        }

        private void ShowAccounts(object sender, RoutedEventArgs e)
        {
            var accountListPage = new AccountList(_client.Id);
            NavigationService?.Navigate(accountListPage);
        }
    }
}