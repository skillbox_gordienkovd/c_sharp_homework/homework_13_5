﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using homework_13_5.UI.Account.Add;
using homework_13_5.UI.Account.Debit;
using homework_13_5.UI.Account.Withdrawal;
using static homework_13_5.UI.Utility.Utility;

namespace homework_13_5.UI.Account.List
{
    public partial class AccountList
    {
        private readonly long _clientId;
        private ListSortDirection _lastDirection = ListSortDirection.Ascending;
        private GridViewColumnHeader _lastHeaderClicked;

        public AccountList()
        {
        }

        public AccountList(long id)
        {
            _clientId = id;
            InitializeComponent();
            AccountListBox.ItemsSource = Repository.Repository.AccountRepository.FindByClientId(id);
            AccountListBox.Items.Refresh();
        }

        private void DebitClick(object sender, RoutedEventArgs e)
        {
            var id = (Guid) ((Button) sender).Tag;
            var accountDebitPage = new AccountDebit(id);
            NavigationService?.Navigate(accountDebitPage);
        }

        private void WithdrawalClick(object sender, RoutedEventArgs e)
        {
            var id = (Guid) ((Button) sender).Tag;
            var accountWithdrawalPage = new AccountWithdrawal(id);
            NavigationService?.Navigate(accountWithdrawalPage);
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            var id = (Guid) ((Button) sender).Tag;
            Repository.Repository.AccountRepository.Delete(id);
            AccountListBox.Items.Refresh();
            NotifyUser($"Счет {id} закрыт");
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            var accountAddPage = new AccountAdd(_clientId);
            var navigationService = NavigationService;
            navigationService?.Navigate(accountAddPage);
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked == null) return;

            if (headerClicked.Role == GridViewColumnHeaderRole.Padding) return;

            if (headerClicked != _lastHeaderClicked)
                direction = ListSortDirection.Ascending;
            else
                direction = _lastDirection == ListSortDirection.Ascending
                    ? ListSortDirection.Descending
                    : ListSortDirection.Ascending;

            var columnBinding = headerClicked.Column.DisplayMemberBinding as Binding;
            var sortBy = columnBinding?.Path.Path ?? headerClicked.Column.Header as string;

            if (sortBy == null)
            {
                return;
            }
            
            AccountListBox.Items.SortDescriptions.Clear();
            var sd = new SortDescription(sortBy, direction);
            AccountListBox.Items.SortDescriptions.Add(sd);
            AccountListBox.Items.Refresh();


            _lastHeaderClicked = headerClicked;
            _lastDirection = direction;
        }
    }
}