﻿using System;
using System.Windows;
using homework_13_5.UI.Account.List;
using static homework_13_5.UI.Utility.Utility;

namespace homework_13_5.UI.Account.Debit
{
    public partial class AccountDebit
    {
        private readonly Domain.Account _account;

        public AccountDebit(Guid number)
        {
            _account = Repository.Repository.AccountRepository.FindByNumber(number);
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs routedEventArgs)
        {
            double.TryParse(DebitAmount.Text, out var amount);

            if (amount <= 0)
            {
                DebitAmount.Clear();

                return;
            }

            _account.Balance += amount;
            Repository.Repository.AccountRepository.Save(_account);
            NotifyUser($"Баланс пополнен на {amount}");
            NavigationService?.Navigate(new AccountList(_account.ClientId));
        }

        
    }
}