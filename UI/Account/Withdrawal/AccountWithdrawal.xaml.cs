﻿using System;
using System.Windows;
using homework_13_5.Exception;
using homework_13_5.UI.Account.List;
using static homework_13_5.UI.Utility.Utility;

namespace homework_13_5.UI.Account.Withdrawal
{
    public partial class AccountWithdrawal
    {
        private readonly Domain.Account _account;

        public AccountWithdrawal(Guid number)
        {
            _account = Repository.Repository.AccountRepository.FindByNumber(number);
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs routedEventArgs)
        {
            var isSuccessParsedAmount = double.TryParse(WithdrawalAmount.Text, out var amount);

            if (!isSuccessParsedAmount)
            {
                NotifyUser("Сумма для перевода средств указана неверно");
                return;
            }
            
            try
            {
                CheckFundsForWithdrawal(_account, amount);
            }
            catch (NotEnoughFundsException e)
            {
                NotifyUser("Недостаточно средств для перевода.");
                return;
            }

            var isSuccessParsedNumber = Guid.TryParse(WithdrawalNumber.Text, out var number);

            if (!isSuccessParsedNumber)
            {
                NotifyUser("Номер счета для перевода средств указан неверно");
                return;
            }

            Domain.Account withdrawalAccount;
            
            try
            {
               withdrawalAccount = Repository.Repository.AccountRepository.FindByNumber(number);
            }
            catch (System.Exception e)
            {
                NotifyUser("Указанный номер счета для перевода средств не существует.");
                return;
            } 

            _account.Balance -= amount;
            withdrawalAccount.Balance += amount;

            Repository.Repository.AccountRepository.Save(_account);
            Repository.Repository.AccountRepository.Save(withdrawalAccount);

            NotifyUser($"Перевод осуществлен на сумму: {amount}");
            NavigationService?.Navigate(new AccountList(_account.ClientId));
        }

        private static void CheckFundsForWithdrawal(Domain.Account account, double amount)
        {
            if (account.Balance + account.CreditLimit < amount)
            {
                throw new NotEnoughFundsException("Недостаточно средств на балансе");
            }
        }
    }
    
}