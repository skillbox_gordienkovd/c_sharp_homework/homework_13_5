﻿using System.Windows;
using homework_13_5.Domain.enumeration;
using homework_13_5.UI.Account.List;

namespace homework_13_5.UI.Account.Add
{
    public partial class AccountAdd
    {
        public Domain.Account _account = new Domain.Account();

        public AccountAdd(long clientId)
        {
            _account.ClientId = clientId;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            var accountType = (AccountType) AccountType.SelectedItem;

            if (accountType == Domain.enumeration.AccountType.Credit)
            {
                double.TryParse(AccountCreditLimit.Text, out var creditLimit);
                if (creditLimit < 0)
                    return;
                _account.CreditLimit = creditLimit;
            }
            else
            {
                _account.CreditLimit = 0;
            }

            _account.Balance = 0;
            _account.AccountType = (AccountType) AccountType.SelectedItem;

            Repository.Repository.AccountRepository.Save(_account);
            
            NavigationService?.Navigate(new AccountList(_account.ClientId));
        }
    }
}