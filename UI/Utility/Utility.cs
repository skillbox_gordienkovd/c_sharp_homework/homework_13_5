﻿using System;
using System.Windows;

namespace homework_13_5.UI.Utility
{
    public static class Utility
    {
        public static event Action<string> UserNotification;
        
        static Utility()
        {
            UserNotification += ShowUserNotification;
        }

        public static void NotifyUser(string message)
        {
            UserNotification?.Invoke(message);
        }
        
        private static void ShowUserNotification(string message)
        {
            MessageBox.Show(message);
        }
    }
}