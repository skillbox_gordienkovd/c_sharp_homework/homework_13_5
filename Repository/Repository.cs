﻿using System.IO;
using System.Text.Json;

namespace homework_13_5.Repository
{
    public static class Repository
    {
        public static readonly SequenceRepository SequenceRepository = new SequenceRepository();
        public static ClientRepository ClientRepository = new ClientRepository();
        public static AccountRepository AccountRepository = new AccountRepository();


        static Repository()
        {
            InitClientRepository();
            InitAccountRepository();
        }

        private static void InitClientRepository()
        {
            if (File.Exists("clients_db.json"))
                ClientRepository =
                    JsonSerializer.Deserialize<ClientRepository>(File.ReadAllText("clients_db.json"));
        }

        private static void InitAccountRepository()
        {
            if (File.Exists("accounts_db.json"))
                AccountRepository =
                    JsonSerializer.Deserialize<AccountRepository>(File.ReadAllText("accounts_db.json"));
        }
    }
}