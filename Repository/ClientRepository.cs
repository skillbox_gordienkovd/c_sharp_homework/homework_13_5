﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;
using homework_13_5.Domain;

namespace homework_13_5.Repository
{
    public class ClientRepository
    {
        public ObservableCollection<Client> Clients { get; set; } =
            new ObservableCollection<Client>();

        public Client Save(Client client)
        {
            if (client.Id == 0)
            {
                client.Id = Repository.SequenceRepository.ClientId();
                Clients.Add(client);
                SaveDb();
            }
            else
            {
                var found = FindById(client.Id);

                if (found != null)
                {
                    found.Firstname = client.Firstname;
                    found.Lastname = client.Lastname;
                    SaveDb();
                }
                else
                {
                    client.Id = Repository.SequenceRepository.ClientId();
                    SaveDb();
                }
            }

            return client;
        }

        public void Delete(long id)
        {
            Clients.Remove(FindById(id));
            SaveDb();
        }

        public Client FindById(long id)
        {
            return Clients.Single(item => item.Id == id);
        }

        public IEnumerable<Client> FindAll()
        {
            return Clients;
        }

        private void SaveDb()
        {
            File.WriteAllText("clients_db.json", JsonSerializer.Serialize(this));
        }
    }
}