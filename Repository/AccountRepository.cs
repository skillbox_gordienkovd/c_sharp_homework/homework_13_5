﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;
using homework_13_5.Domain;

namespace homework_13_5.Repository
{
    public class AccountRepository
    {
        public ObservableCollection<Account> Accounts { get; set; } =
            new ObservableCollection<Account>();

        public Account Save(Account account)
        {
            if (account.Number == Guid.Empty)
            {
                account.Number = Repository.SequenceRepository.AccountGuid();
                Accounts.Add(account);
                SaveDb();
            }
            else
            {
                var found = FindByNumber(account.Number);

                if (found != null)
                {
                    found.Balance = account.Balance;
                    found.CreditLimit = account.CreditLimit;
                    SaveDb();
                }
                else
                {
                    account.Number = Repository.SequenceRepository.AccountGuid();
                    SaveDb();
                }
            }

            return account;
        }

        public void Delete(Guid number)
        {
            Accounts.Remove(FindByNumber(number));
            SaveDb();
        }

        public Account FindByNumber(Guid number)
        {
            return Accounts.Single(item => item.Number == number);
        }

        public IEnumerable<Account> FindAll()
        {
            return Accounts;
        }

        public IEnumerable<Account> FindByClientId(long id)
        {
            return Accounts.Where(account => account.ClientId == id);
        }

        private void SaveDb()
        {
            File.WriteAllText("accounts_db.json", JsonSerializer.Serialize(this));
        }
    }
}