﻿using System;
using System.IO;
using System.Text.Json;
using homework_13_5.Domain;

namespace homework_13_5.Repository
{
    public class SequenceRepository
    {
        private readonly Sequence _sequence = new Sequence();

        public SequenceRepository()
        {
            if (File.Exists("sequence_db.json"))
                _sequence = JsonSerializer.Deserialize<Sequence>(File.ReadAllText("sequence_db.json"));
        }

        public long ClientId()
        {
            var id = _sequence.SequenceClientId();
            Save();

            return id;
        }

        public Guid AccountGuid()
        {
            return _sequence.SequenceAccountGuid();
        }

        private void Save()
        {
            File.WriteAllText("sequence_db.json", JsonSerializer.Serialize(_sequence));
        }
    }
}