using System;

namespace homework_13_5.Domain
{
    public class Sequence
    {
        public long ClientId { get; set; }

        public Guid AccountGuid { get; set; }

        public long SequenceClientId()
        {
            return ++ClientId;
        }

        public Guid SequenceAccountGuid()
        {
            return Guid.NewGuid();
        }
    }
}