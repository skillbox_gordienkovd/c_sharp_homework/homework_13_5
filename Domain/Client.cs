﻿namespace homework_13_5.Domain
{
    public class Client
    {
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}