﻿using System;
using homework_13_5.Domain.enumeration;

namespace homework_13_5.Domain
{
    public class Account
    {
        public Guid Number { get; set; }

        public double Balance { get; set; }

        public long ClientId { get; set; }

        public AccountType AccountType { get; set; }

        public double CreditLimit { get; set; }
    }
}