﻿namespace homework_13_5.Exception
{
    public class NotEnoughFundsException : System.Exception
    {
        public NotEnoughFundsException(string message) : base(message)
        {
        }
    }
}